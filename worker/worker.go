package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	pb "gitlab.com/lakshanwd/go-job-queue/mail"
	"google.golang.org/grpc"
)

var address string
var maxDeamonCount int
var quit chan bool

//config for worker
type workerConfig struct {
	Address        string `json:"address"`
	MaxDeamonCount int    `json:"max-deamon-count"`
}

type deamon struct {
	ID int
}

func newDeamon(id int) *deamon {
	return &deamon{ID: id}
}

func (d *deamon) start(ctx context.Context, mailClient pb.MailClient, worker *pb.Worker) {
	go func(ctx context.Context) {
		for {
			select {
			case <-ctx.Done():
				return
			default:
				if email, err := mailClient.GetEmail(ctx, worker); err != nil {
					send(email, d.ID)
				} else {
					quit <- true
				}
			}
		}
	}(ctx)
}

func send(e *pb.EmailRequest, deamonID int) {
	time.Sleep(time.Millisecond * 100)
	log.Printf("email %v sent by deamon %v\n", e.GetTitle(), deamonID)
}

func main() {
	//setup configuration
	config := loadConfiguration()
	address = config.Address
	maxDeamonCount = config.MaxDeamonCount

	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("unable to connect: %v", err.Error())
	}
	defer conn.Close()
	log.Println("connected")

	//initializing server
	mailClient := pb.NewMailClient(conn)
	worker := &pb.Worker{WorkerName: os.Args[1]}

	//create deamons
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	for i := 0; i < maxDeamonCount; i++ {
		deamon := newDeamon(i)
		deamon.start(ctx, mailClient, worker)
	}

	//waiting for deamons to quit
	for i := 0; i < maxDeamonCount; i++ {
		<-quit
	}
}

func loadConfiguration() workerConfig {
	file := "./config.json"
	var config workerConfig
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		fmt.Println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	return config
}
