FROM golang:latest

WORKDIR /go/src/gitlab.com/lakshanwd/go-job-queue

COPY client/client.go ./client/client.go
COPY mail ./mail
RUN go get -d ./... && go install -v ./...
COPY client/config.prod.json ./config.json
CMD [ "client" ]